package ru.terrakok.gitlabclient.entity

/**
 * Created by Eugene Shapovalov (@CraggyHaggy) on 04.01.19.
 */
data class Color(
    val name: String,
    val value: Int
)
