package ru.terrakok.gitlabclient.entity.app.develop

/**
 * Created by Konstantin Tskhovrebov (aka @terrakok) on 03.12.17.
 */
enum class LicenseType {
    MIT,
    APACHE_2,
    CUSTOM,
    NONE
}
