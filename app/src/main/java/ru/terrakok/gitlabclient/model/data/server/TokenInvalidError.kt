package ru.terrakok.gitlabclient.model.data.server

/**
 * @author Eugene Shapovalov (@CraggyHaggy) on 14.09.18.
 */
class TokenInvalidError : RuntimeException()
